#!/usr/bin/env python3
import api

'''
Sets mailboxes to infinity if they are 90%
'''


def main():
    mailboxes = api.get_mailbox("all")
    mailboxes_with_out_infinty = [m for m in mailboxes if m["quota"] !=0]
    mailboxes_nearly_full = [m["username"] for m in mailboxes_with_out_infinty if m["percent_in_use"] > 90]
    print(api.edit_mailbox(mailbox = mailboxes_nearly_full, attr = {"quota": 0}))


if __name__ == "__main__":
    main()
