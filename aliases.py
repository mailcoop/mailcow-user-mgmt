#!/usr/bin/env python3

import api
import csv

DOMAIN = "3-c.coop"

def read_aliases_file(aliases="aliases.csv"):
    ''' 
    reads a csv and returns a list of tuples (destinations, sources)
    '''
    out = []
    with open(aliases, encoding='utf-8-sig') as csvfile:
        data_import = csv.DictReader(csvfile)
        data = list(data_import)
    for line in data:
        mailbox, alias = line["mailbox"].strip().lower(), line["alias"].strip().lower()
        out.append((mailbox,alias))
    return out

def main():
    mailboxes = [m["username"] for m in api.get_mailbox("all")]
    aliases = read_aliases_file()
    for mailbox, alias in aliases:
        if mailbox in mailboxes:
            alias_config = api.generate_alias_config(address=alias, goto=mailbox)
            print(api.add_alias(alias_config))
        else:
            print("NO MAILBOX:" + mailbox)
    
    


     

if __name__ == "__main__":
    main()
