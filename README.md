# Some notes. 

- api.py:
    - wrapper for mailcow api

- passwords.py:
    - generate nice passwords for people

- emailer.py:
    - sends emails to people using a jinja2 templates

- get_text_from_html:
    - generates a text template from an html one. 
    - template names are hardcoded.. just edit it. 

- datareader.py:
    - reads the csv and generates a big list of dictionaries to be used in other things.
    - headers in the csv need to be correct
    - file encoding needs to be correct too
    - can run it on it's own to see that things are parsing properly.

- create.py:
    - Reads a file called "import.csv" (see import.test.csv and import.example.csv) 
    - gets a nice password for each user and creates an a/c and emails details to their legacy a/c. Also Bcc's the account in config.py
    - BEFORE YOU RUN IT DO SOME TEST.  
    - BEFORE YOU RUN IT run `datareader.py` by itself to tell if the import.csv parses properly (the excel output is not always good, and you might want to run dos2unix on it)
    - BEFORE YOU RUN IT check for duplicates `cut -d, -f4 import.csv  | while read i; do grep ,$i, imports/imported_cumulative.csv ; done | less`   
    - the headers in the csv need to match properly
    - run it and redirect the output to a log file example `create.py >> /some/log/file` to keep a record 
    - AFTER running it copy import.csv to the imports/ directory (and rename to something sensible with a date for example)
    - AFTER running append the import.csv to "imported_cumulative.csv" file `cat import.csv >> imports/imported_cumulative.csv`
    
- syncjobs.py:
    - Some snippets to get stats on syncjobs.  Have a look and comment out/in the bits you want.
    - Can lookup against imports/imported_cumulative.csv to check the legacy addresses/order numbers

# TODO

- needs a proper logger writing
- needs the template being used factoring out of datareader.py 
- check to strip whitespace from csv import
- need to write something to do SOGo configuration also.
