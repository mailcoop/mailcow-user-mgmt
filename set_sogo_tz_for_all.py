#!/usr/bin/env python3

import api,sql

def main():
    mboxs = api.get_mailbox("all")
    for m in mboxs:
        sprofile = sql.get_sogo_profile(user=m["username"])
        if sprofile is not None and sprofile[1] is not None:
            print(sql.set_sogo_tz_profile_eu_london(profile = sprofile))


if __name__ == "__main__":
    main()
