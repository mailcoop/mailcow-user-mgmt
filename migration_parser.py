#!/usr/bin/env python3

import config
import csv
import password
import os 

DIR  = os.path.dirname(__file__)
IMPORT_FILE = os.path.join(DIR, "import.csv")

def import_data_dict(import_file = IMPORT_FILE):
    with open(import_file, encoding='utf-8-sig') as csvfile:
        data_import = csv.DictReader(csvfile)
        data = list(data_import)

    return data

def generate_required_data_dict(data = import_data_dict()):
    for row in data:
        row["password"] = password.passphrase()
        # check to make sure fullname isn't going to be blank
        if row["givenname"] == "" and row["sn"] == "":
            row["full_name"] = row["local_part"]
        else:
            row["full_name"] = row["givenname"] + " " + row["sn"]
        row["email"] = row["local_part"] + "@" + config.LOCAL_DOMAIN
        row["Subject"] = "Welcome to Mail Coop!"
        row["template_txt"] =  "welcome_email_7.txt.j2"
        row["template_html"] = "welcome_email_7.html.j2"
    return data


def main():
    print(generate_required_data_dict())

if __name__ == '__main__':
    main()
