#!/usr/bin/env python3
import config
import json
import MySQLdb

DBUSER=config.DBUSER
DBPASS=config.DBPASS
DBNAME=config.DBNAME

def connect(host=config.DBHOST, port=config.DBPORT, user=DBUSER, password=DBPASS, database=DBNAME):
    return MySQLdb.connect(host=host, port=port, user=user, password=password, database=database)

def get_sogo_profile(db=connect(), user="hello@mail.coop"):
    query = "select * from sogo_user_profile where c_uid=%s"
    c = db.cursor()
    c.execute(query, (user,))
    return c.fetchone()

def set_sogo_tz_profile_eu_london(db=connect(), profile="hello@mail.coop"):
    '''
    if the sogo profile is Etc/UTC then set it to Europe/London
    '''
    out = ": Nothing"
    sogo_prefs = json.loads(profile[1])
    if sogo_prefs["SOGoTimeZone"] == "Etc/UTC":
        sogo_prefs["SOGoTimeZone"] = "Europe/London"
        query = "update sogo_user_profile set c_defaults = %s where c_uid = %s"
        c = db.cursor()
        c.execute(query, (json.dumps(sogo_prefs), profile[0]))
        db.commit()
        out = ": Changed"
    return profile[0] + out

def get_password_hash(db=connect(), username="kate@mailcoop.uk"):
    query = "select password from mailbox where username=%s"
    c = db.cursor()
    c.execute(query, (username,))
    return c.fetchone()[0]

def set_password_hash(db=connect(), password="password" ,username="kate@mailcoop.uk"):
    query = "update mailbox set password = %s where username = %s"
    c = db.cursor()
    c.execute(query, (password, username))
    db.commit()
    return c.rowcount
    
def get_imapsync_settings(db=connect(), sync_id=None):
    query = "select host1, authmech1, regextrans2, authmd51, domain2, subfolder2, user1, password1, exclude, maxage, mins_interval, maxbytespersecond, port1, enc1, delete2duplicates, delete1, delete2, automap, skipcrossduplicates, custom_params, timeout1, timeout2, subscribeall, dry, active  from imapsync where id = %s"
    c = db.cursor(cursorclass=MySQLdb.cursors.DictCursor)
    c.execute(query, (sync_id,))
    return c.fetchone()

#
def main():
    db = connect()
    print("original settings")
    for username in ["kate@mailcoop.uk", "kate@test.example"]:
        print(username, get_password_hash(db=db, username=username))
    print("make changes")
    source_pass = get_password_hash(db=db, username="kate@mailcoop.uk")
    set_password_hash(db, password=source_pass, username="kate@test.example")
    print("new values")
    for username in ["kate@mailcoop.uk", "kate@test.example"]:
        print(username, get_password_hash(db=db, username=username))
#
if __name__ == '__main__':
    main()

