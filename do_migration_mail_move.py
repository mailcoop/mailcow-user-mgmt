#!/usr/bin/env ipython3

import datetime
import migration_parser
import os
import shutil
import sys

ROOTPATH="/var/lib/docker/volumes/mailcowdockerized_vmail-vol-1/_data"

def main():
    if os.geteuid() != 0:
        print("You need to have root privileges to run this script.\nPlease try again, this time using 'sudo'. Exiting.")
        sys.exit()
    for data in migration_parser.generate_required_data_dict():
        dt = datetime.datetime.now().strftime("%Y%m%d%H%M%S")
        print(dt, data["email"], data["import_email"])
        source_user,source_domain = data["email"].split("@")
        source_path = ROOTPATH + "/" + source_domain + "/" + source_user
        dest_user,dest_domain = data["import_email"].split("@")
        dest_path = ROOTPATH + "/" + dest_domain + "/" + dest_user
        print(dt, "move ", source_path, dest_path)
        shutil.move(source_path, dest_path)


if __name__ == "__main__":
    main()

