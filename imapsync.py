#!/usr/bin/env python3

import api
import csv


DOMAIN="phonecoop.coop"
DEFAULT_SYNC_SETTINGS = {'host1': 'imap.myphone.coop',
 'authmech1': 'PLAIN',
 'regextrans2': '',
 'authmd51': 0,
 'domain2': '',
 'subfolder2': '',
 'user1': None,
 'password1': None,
 'exclude': '(?i)spam|(?i)junk|(?i)trash',
 'maxage': 0,
 'mins_interval': 20,
 'maxbytespersecond': '0',
 'port1': 143,
 'enc1': 'TLS',
 'delete2duplicates': 1,
 'delete1': 0,
 'delete2': 0,
 'automap': 1,
 'skipcrossduplicates': 0,
 'custom_params': '',
 'timeout1': 600,
 'timeout2': 600,
 'subscribeall': 1,
 'dry': 0,
 'active': 1}

def read_password_file(domain=DOMAIN):
    ''' 
    reads a csv and returns a dictionary keyed by email
    '''
    out = {}
    with open("passwords/" + domain + "/passwords.csv", encoding='utf-8-sig') as csvfile:
        data_import = csv.DictReader(csvfile)
        data = list(data_import)
    for line in data:
        mailbox, password = line["email"], line["password"]
        out[mailbox] = password
    return out

def get_mailboxes(domain=DOMAIN):
    '''
    returns a list a mailboxes in a domain
    '''
    return [mailbox["username"] for mailbox in api.get_mailbox("all") if mailbox["domain"] == domain]

def lookup_password(mailbox, passwords):
    return passwords[mailbox]

def add_imapsync(mailbox):
    domain = mailbox.split("@")[1]
    print(domain)
    passwords = read_password_file(domain)
    if mailbox in passwords:
        # print(mailbox, passwords[mailbox])
        imapsync_settings = DEFAULT_SYNC_SETTINGS.copy()
        imapsync_settings["username"] = mailbox
        imapsync_settings["user1"] = mailbox
        imapsync_settings["password1"] = passwords[mailbox]
        out = api.add_syncjob(imapsync_settings)
    else: 
        out = (mailbox, "NO PASSWORD")
    return out
 
def main():
    #syncjobs = api.get_sync_jobs()
    #print(DEFAULT_SYNC_SETTINGS)
    #print(len(syncjobs))
    #domain_sync_jobs = [job["id"] for job in syncjobs if len(job["user1"].split("@")) == 2 and job["user1"].split("@")[1] == DOMAIN]
    #print(domain_sync_jobs)
    #print(api.delete_syncjobs(domain_sync_jobs))
    passwords = read_password_file()
    mailboxes = get_mailboxes()
    for mailbox in mailboxes:
        if mailbox in passwords:
            # print(mailbox, passwords[mailbox])
            imapsync_settings = DEFAULT_SYNC_SETTINGS.copy()
            imapsync_settings["username"] = mailbox
            imapsync_settings["user1"] = mailbox
            imapsync_settings["password1"] = passwords[mailbox]
            print(api.add_syncjob(imapsync_settings))
        else: 
            print(mailbox, "NO PASSWORD")

     

if __name__ == "__main__":
    main()
